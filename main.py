import requests

from flask import Flask, request
from conf import HOST, PORT, URL

app = Flask(__name__)


@app.route('/whois')
def whois():
    ip = request.args.get('ip')
    r = requests.get(URL + ip)
    return r.json()


if __name__ == '__main__':
    app.run(debug=True, host=HOST, port=PORT)
